class RateWidget {
  constructor() {
    this.items = document.querySelectorAll('.rate-widget__item');
    this.container = document.querySelectorAll('.container');
    this.popup = document.querySelectorAll('.popup');
    this.popupFastItems = document.querySelectorAll('.popup-fast__item');
    this.form = document.querySelectorAll('#form');
    this.cookie = document.cookie;
    this.index = this.getCookie('rateWidgetIndex');
    this.popupShow = this.getCookie('rateWidgetPopup');
  }

  setCookie(name, value, exdays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value = escape(value) + ((exdays == null) ? '' : '; expires=' + exdate.toUTCString());
    document.cookie = name + '=' + c_value;
  }

  getCookie(name) {
    var i, x, y, ARRcookies = document.cookie.split(';');
    for (i = 0; i < ARRcookies.length; i++)
    {
        x = ARRcookies[i].substr(0, ARRcookies[i].indexOf('='));
        y = ARRcookies[i].substr(ARRcookies[i].indexOf('=') + 1);
        x = x.replace(/^\s+|\s+$/g, '');
        if (x === name)
        {
            return unescape(y);
        }
    }
  }

  checkRate() {
    window.addEventListener('load', (event) => {
      Array.from(this.items).forEach((item, index) => {
        if(+this.index >= index) {
          item.classList.add('rate-widget__item-active');
        } else {
          item.classList.remove('rate-widget__item-active');
        }
      })
    });
  }

  closePopUp() {
    this.form.forEach(el => el.addEventListener('submit', event => {
      this.setCookie('rateWidgetPopup','false', 3);
      event.preventDefault();
      Array.from(this.popup).forEach((item) => {
        item.classList.remove('popup_active');
      });
    }))
  }

  choseFastAnswer() {
    this.popupFastItems.forEach(el => el.addEventListener('click', event => {
      event.preventDefault();
      event.target.classList.toggle('popup-fast__item_active');
    }))
  }

  setRate() {
    this.items.forEach(el => el.addEventListener('click', event => {
      const voted = this.getCookie('rateWidget');
      if(!voted) {
        Array.from(this.items).forEach((item, index) => {
          if(+event.target.dataset.rate > index) {
            this.setCookie('rateWidget','true', 3);
            this.setCookie('rateWidgetIndex',index, 3);
            item.classList.add('rate-widget__item-active');
          } else {
            item.classList.remove('rate-widget__item-active');
          }
        })
        if(+event.target.dataset.rate < 4) {
          Array.from(this.popup).forEach(item => {
            item.classList.add('popup_active');
          })
        }
      }
    }));
  }
}
let rate = new RateWidget();

rate.setRate();
rate.closePopUp();
rate.checkRate();
rate.choseFastAnswer();